%PLOTTER plots the results obtained from the simulink file pbpk_control.
%% Main Plot
factor=1E6;
figure(814324);
set(0,'DefaultAxesFontName', 'Arial')
set(0,'DefaultAxesFontSize', 12)

do_plot_observer=0;
time=pbpk_resp.time;
time2 = aug_state.time;
response=pbpk_resp.signals.values*factor;
response_hat = aug_state.signals.values*factor;

% Liver
subplot(2,4,1);hold on;
title('Liver tissue');
plot(time, response(:,10));
if do_plot_observer, stairs(time2, response_hat(:,10),'r --'); end
axis ([0 8 0 max(response(:,10))*1.05]);
xlabel('Time (hr)');ylabel('Concentration (ug/L)');grid on;

% Kidney
subplot(2,4,2);hold on;
title('Kidney tissue');
plot(time, response(:,14))
if do_plot_observer, stairs(time2, response_hat(:,14),'r --'); end
axis ([0 8 0 max(response(:,14))*1.05]);
xlabel('Time (hr)');ylabel('Concentration (ug/L)');grid on;

% Plasma
subplot(2,4,3); hold on;
title('Plasma');
plot(time, response(:,1))
if do_plot_observer, stairs(time2, response_hat(:,1),'r --'); end
axis ([0 8 0 max(response(:,1))*1.05]);
plot(c_pl.time,c_pl.signals.values(:,1)*factor,'r --');%reference
xlabel('Time (hr)');ylabel('Concentration (ug/L)');grid on;

% Skin
subplot(2,4,4);hold on;
title('Skin tissue');
plot(time, response(:,6))
if do_plot_observer, stairs(time2, response_hat(:,6),'r --'); end
axis ([0 8 0 max(response(:,6))*1.05]);
xlabel('Time (hr)');ylabel('Concentration (ug/L)');grid on;

%SECOND ROW

%Bladder
subplot(2,4,5);hold on;
title('Bladder tissue');
plot(time, response(:,8))
if do_plot_observer, stairs(time2, response_hat(:,8),'r --'); end
axis ([0 8 0 max(response(:,8))*1.05]);
xlabel('Time (hr)');ylabel('Concentration (ug/L)');grid on;

%Bladder
subplot(2,4,6);hold on;
title('Lung tissue');
plot(time, response(:,4))
if do_plot_observer, stairs(time2, response_hat(:,4),'r --'); end
axis ([0 8 0 max(response(:,4))*1.05]);
xlabel('Time (hr)');ylabel('Concentration (ug/L)');grid on;


subplot(2,4,7);hold on;
title('Residual tissue');
plot(time, response(:,12))
if do_plot_observer, stairs(time2, response_hat(:,12),'r --'); end
axis ([0 8 0 max(response(:,12))*1.05]);
xlabel('Time (hr)');ylabel('Concentration (ug/L)');grid on;

subplot(2,4,8);hold on;
title('RBC');
plot(time, response(:,3))
if do_plot_observer, stairs(time2, response_hat(:,3),'r --'); end
axis ([0 8 0 max(response(:,3))*1.05]);
xlabel('Time (hr)');ylabel('Concentration (ug/L)');grid on;
%% Everything in one plot
figure(15);
hold on;
factor=1E6;
time=pbpk_resp.time;
response=pbpk_resp.signals.values*factor;

plot(c_pl.time,c_pl.signals.values(:,1)*factor,'r --');%reference
plot(time,response(:,1),'Linewidth',2);%plasma
plot(time,response(:,3),'--','Linewidth',2);%rbc
plot(time,response(:,4),'c','Linewidth',2);%lung
plot(time,response(:,6),'g','Linewidth',2);%skin tissue
plot(time,response(:,14),'m','Linewidth',2);%kidney tissue
plot(time,response(:,10),'--','Color',[1,0.1,0.8],'Linewidth',2);%liver tissue
plot(time,response(:,12),'-.','Color',[1,0.8,0.2],'Linewidth',2);%residual tissue
plot(time,response(:,8),'-','Color',[1,0.1,0.9],'Linewidth',2);%bladder tissue

legend('Reference','Plasma','RBC','Lungs','Skin (tissue)','Kidney (tissue)',...
    'Liver (tissue)','Residual (tissue)','Bladder (tissue)',...
    'Location','NorthWest');
xlabel('Time (hr)');
ylabel('Concentration (ug/L)');
grid on;
axis tight;
hold off;

%% Plot the input
figure(7);
stairs(dose.time, dose.signals.values*factor,'Linewidth',2);
xlabel('Time (hr)');
ylabel('Administration Rate (ug/hr)');
axis([-0.01 8 0 0.05]);
grid on;
hold off;