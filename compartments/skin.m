function [sys,x0,str,Tvector]=skin(t,x,u,flag,pbpk)
%SKIN
% PBPK Modelling of Skin (Tissue + Plasma Subcompartments)
% Do: Run first PBPK.m

switch flag,   
case 0,
   [sys,x0,str,Tvector]=zz_mdlInitializeSizes(1,2,2);
case 1,   
   sys=mdlDerivatives(t,x,u,pbpk);
case 3,
   sys=mdlOutputs(t,x,u);
case {2,4},
   sys=[];
case 9,
   mdlTerminate   
otherwise
   error(['Unhandled flag = ',num2str(flag)]);   
end


function sys=mdlDerivatives(t,x,u,pbpk)
C_art=u(1);
C_v_skin=x(1);
C_skin=x(2);
sys(1)=(1/pbpk.blood_skin)*(...
    pbpk.skin_flow*(C_art-C_v_skin)-...
    pbpk.pi_skin*(C_v_skin-C_skin/pbpk.P_skin));
sys(2)=(pbpk.pi_skin/pbpk.skin_volume)*(C_v_skin-C_skin/pbpk.P_skin);


function sys=mdlOutputs(t,x,u)
sys(1)=x(1);
sys(2)=x(2);


function mdlTerminate