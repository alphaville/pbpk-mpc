function [sys,x0,str,Tvector]=kidney(t,x,u,flag,pbpk)
%KIDNEY
% PBPK Modelling of Kidneys (Tissue + Plasma Subcompartments)
% Do: Run first PBPK.m

switch flag,   
case 0,
   [sys,x0,str,Tvector]=zz_mdlInitializeSizes(1,2,2);
case 1,   
   sys=mdlDerivatives(t,x,u,pbpk);
case 3,
   sys=mdlOutputs(t,x,u);
case {2,4},
   sys=[];
case 9,
   mdlTerminate   
otherwise
   error(['Unhandled flag = ',num2str(flag)]);   
end


function sys=mdlDerivatives(t,x,u,pbpk)
C_art=u(1);
C_v_kidney=x(1);
C_kidney=x(2);
sys(1)=(1/pbpk.blood_kidney)*(...
    pbpk.kidney_flow*(C_art-C_v_kidney)-...
    pbpk.pi_kidney*(C_v_kidney-C_kidney/pbpk.P_kidney)-...
    pbpk.k_kindey*pbpk.blood_kidney*C_v_kidney);
sys(2)=(pbpk.pi_kidney/pbpk.kidney_volume)*...
    (C_v_kidney-C_kidney/pbpk.P_kidney);


function sys=mdlOutputs(t,x,u)
sys(1)=x(1);
sys(2)=x(2);


function mdlTerminate