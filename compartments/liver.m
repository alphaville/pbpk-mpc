function [sys,x0,str,Tvector]=liver(t,x,u,flag,pbpk)
%LIVER
% PBPK Modelling of Liver (Tissue + Plasma Subcompartments)
% Do: Run first PBPK.m

switch flag,
    case 0,
        [sys,x0,str,Tvector]=zz_mdlInitializeSizes(1,2,2);
    case 1,
        sys=mdlDerivatives(t,x,u,pbpk);
    case 3,
        sys=mdlOutputs(t,x,u);
    case {2,4},
        sys=[];
    case 9,
        mdlTerminate
    otherwise
        error(['Unhandled flag = ',num2str(flag)]);
end


function sys=mdlDerivatives(t,x,u,pbpk)
C_art=u(1);
C_v_liver=x(1);
C_liver=x(2);
sys(1)=(1/pbpk.blood_liver)*(...
    pbpk.liver_flow*(C_art-C_v_liver)-...
    pbpk.pi_liver*(C_v_liver-C_liver/pbpk.P_liver)  );
sys(2)=(1/pbpk.liver_volume)*(...
    pbpk.pi_liver*(C_v_liver-C_liver/pbpk.P_liver)-...
    C_liver*pbpk.liver_volume*(pbpk.k_met+pbpk.k_bile));


function sys=mdlOutputs(t,x,u)
sys(1)=x(1);
sys(2)=x(2);


function mdlTerminate