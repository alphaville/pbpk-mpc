function [sys,x0,str,Tvector]=blood(t,x,u,flag,pbpk)
%BLOOD
% PBPK Modelling of the Blood Comparment which consists of the plasma and
% RBC (red blood cells) subcompartments.
% Do: Run first PBPK.m

switch flag,   
case 0,
   [sys,x0,str,Tvector]=zz_mdlInitializeSizes(6,2,2);
case 1,   
   sys=mdlDerivatives(t,x,u,pbpk);
case 3,
   sys=mdlOutputs(t,x,u);
case {2,4},
   sys=[];
case 9,
   mdlTerminate   
otherwise
   error(['Unhandled flag = ',num2str(flag)]);   
end


function sys=mdlDerivatives(t,x,u,pbpk)
C_v_skin=u(1);
C_v_liver=u(2);
C_v_kidney=u(3);
C_v_bladder=u(4);
C_v_residual=u(5);
ivrate=u(6);
C_plasma=x(1);
C_rbc=x(2);
sys(1)=(1/pbpk.plasma_volume)* (...
    pbpk.skin_flow*C_v_skin...
    +pbpk.liver_flow*C_v_liver...
    +pbpk.kidney_flow*C_v_kidney...
    +pbpk.bladder_flow*C_v_bladder...
    +pbpk.rest_flow*C_v_residual...
    +ivrate...
    +pbpk.pi_rbc*C_rbc...
    -pbpk.pi_plasma*C_plasma...
    -pbpk.cardiac_output*C_plasma);
sys(2)=(1/pbpk.rbc_volume)*(...
    pbpk.pi_plasma*C_plasma...
    -pbpk.pi_rbc*C_rbc);


function sys=mdlOutputs(t,x,u)
sys(1)=x(1);
sys(2)=x(2);

function mdlTerminate