function [sys,x0,str,Tvector]=lung(t,x,u,flag,pbpk)
%LUNG
% PBPK Modelling of Lung (Tissue + Plasma Subcompartments)
% Do: Run first PBPK.m

switch flag,   
case 0,
   [sys,x0,str,Tvector]=zz_mdlInitializeSizes(1,2,2);
case 1,   
   sys=mdlDerivatives(t,x,u,pbpk);
case 3,
   sys=mdlOutputs(t,x,u);
case {2,4},
   sys=[];
case 9,
   mdlTerminate   
otherwise
   error(['Unhandled flag = ',num2str(flag)]);   
end


function sys=mdlDerivatives(t,x,u,pbpk)

C_plasma=u(1);% aka: C_v_lung
C_art=x(1);
C_lung=x(2);


sys(1)=(1/pbpk.blood_lung)*(...
    pbpk.cardiac_output*(C_plasma-C_art)-...
    pbpk.pi_lung*(C_art-C_lung/pbpk.P_lung)  );
sys(2)=(pbpk.pi_lung/pbpk.lung_volume)*(C_art-C_lung/pbpk.P_lung);


function sys=mdlOutputs(t,x,u)
sys(1)=x(1);
sys(2)=x(2);


function mdlTerminate