set(0,'DefaultAxesFontName', 'Helvetica')
set(0,'DefaultAxesFontSize', 12)
set(0,'DefaultTextFontname', 'Helvetica')
set(0,'DefaultTextFontSize', 12)


time = aug_state.time;
ctime = pbpk_resp.time;
hat_signals = aug_state.signals.values*1000;
signals = pbpk_resp.signals.values*1000;



figure(1);
subplot(2,2,1)
hold on;
plot(ctime, signals(:,1),'Linewidth',2);%plasma
stairs(time, hat_signals(:,1),'r');%plasma est
plot(ctime, signals(:,6),'k','Linewidth',2);%skin tissue
stairs(time, hat_signals(:,6),'g');%skin tissue est
hold off;
legend('Plasma','Plasma (estimated)','Skin tissue','Skin tissue (estimated)');
xlabel('Time (hr)');
ylabel('Concentration (mg/L)');
grid on;
axis tight;


subplot(2,2,2)
hold on;
plot(ctime, signals(:,10),'r','Linewidth',2);%Liver
stairs(time, hat_signals(:,10),'k --');%Liver est
plot(ctime, signals(:,3),'Linewidth',2);%Kidney
stairs(time, hat_signals(:,3),'r');%Kidney est
legend('Liver tissue','Liver tissue (estimated)','RBC','RBC (estimated)');
xlabel('Time (hr)');
ylabel('Concentration (mg/L)');
grid on;
axis tight;

subplot(2,2,3)
hold on;
plot(ctime, signals(:,12),'r','Linewidth',2);%Residual tissue
stairs(time, hat_signals(:,12),'k --');%Residual tissue est
plot(ctime, signals(:,8),'c','Linewidth',2);%bladder tissue
stairs(time, hat_signals(:,8),'m');%bladder tissue est
legend('Residual tissue','Residual tissue (estimated)','Bladder tissue','Bladder tissue (estimated)');
xlabel('Time (hr)');
ylabel('Concentration (mg/L)');
grid on;
axis tight;

subplot(2,2,4)
hold on
plot(ctime, signals(:,4),'b --','Linewidth',2);%liver tissue
stairs(time, hat_signals(:,4),'r');%liver tissue est
plot(ctime, signals(:,14),'k ','Linewidth',2);%kidney tissue
stairs(time, hat_signals(:,14),'m');%kidney tissue est
legend('Lung tissue','Lung tissue (estimated)','Kidney tissue','Kidney tissue (estimated)');
xlabel('Time (hr)');
ylabel('Concentration (mg/L)');
grid on;
axis tight;

hold off;

%% Disturbance
figure(9);
stairs(time, hat_signals(:,15),'r','Linewidth',2);%disturbance estimation
xlabel('Time (hr)');
ylabel('Concentration (mg/L)');
grid on;
axis tight;