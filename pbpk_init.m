function pbpk=pbpk_init(bw,hematocrit)
%PBPK Physiological Constants describing ADME characteristics of DMA in mice.
%returns a structure that contains all PK-relevant parameters.
%


%Author: 
%Pantelis Sopasakis

%{
    Offset-free Model Predictive Control for Optimal Drug Administration
    Copyright (C) 2012-2014 Pantelis Sopasakis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

%% Constants
pbpk.bw=bw;%kg (Body Weight) - Range: 0.025~0.030
pbpk.hematocrit=hematocrit;% dimensionless

%% Flows
% Flows of blood through the various organs are given as fractions of the 
% cardiac outflux in L/hr.
pbpk.cardiac_output=16.5;%L/hr
pbpk.liver_flow_factor=16.2/100;%liver flow as percentage wrt cardiac_output
pbpk.kidney_flow_factor=9.1/100;%kidney flow as percentage wrt cardiac_output
pbpk.skin_flow_factor=5.8/100;%skin flow as percentage wrt cardiac_output
pbpk.bladder_flow_factor=0.33/100;%bladder flow as percentage wrt cardiac_output
pbpk.lung_flow_factor=100/100;%the blood flows readily through the lung
pbpk.rest_flow_factor= 1 - pbpk.liver_flow_factor -pbpk.kidney_flow_factor -...
                  pbpk.skin_flow_factor - pbpk.bladder_flow_factor;%the rest...

% Flows in absolute numbers
pbpk.liver_flow=pbpk.liver_flow_factor * pbpk.cardiac_output;%L/hr
pbpk.kidney_flow=pbpk.kidney_flow_factor * pbpk.cardiac_output;%L/hr
pbpk.skin_flow=pbpk.skin_flow_factor * pbpk.cardiac_output;%L/hr
pbpk.bladder_flow=pbpk.bladder_flow_factor * pbpk.cardiac_output;%L/hr
pbpk.lung_flow=pbpk.lung_flow_factor * pbpk.cardiac_output;%L/hr
pbpk.rest_flow=pbpk.rest_flow_factor * pbpk.cardiac_output;%L/hr
              
%% Volumes 1 - Coefficients
% Volumes are given as fractions of the body weight using the following
% coefficients:
pbpk.liver_volume_fraction=5.5/100;%L/kg
pbpk.kidney_volume_fraction=1.7/100;
pbpk.skin_volume_fraction=16.5/100;
pbpk.bladder_volume_fraction=0.09/100;
pbpk.lung_volume_fraction=0.7/100;
pbpk.rest_volume_fraction = 1 - pbpk.liver_volume_fraction - ...
    pbpk.kidney_volume_fraction -...
    pbpk.skin_volume_fraction - ...
    pbpk.bladder_volume_fraction - ...
    pbpk.lung_volume_fraction;
pbpk.blood_volume_fraction=50/100;

% Dimensionells fractions of volume:
pbpk.blood_lung_fraction=0.5;
pbpk.blood_skin_fraction=0.03;
pbpk.blood_liver_fraction=0.31;
pbpk.blood_kidney_fraction=0.24;
pbpk.blood_bladder_fraction=0.03;
pbpk.blood_rest_fraction=0.04;


%% Volumes 2 - Absolute Values

% Volumes of the tissue in absolute values
%{Tissue Volume}={Tissue Vol Fraction}*{BW}*(1-{Blood Fraction in Tissue})
pbpk.liver_volume=pbpk.liver_volume_fraction*pbpk.bw*(1-pbpk.blood_liver_fraction); %L
pbpk.lung_volume=pbpk.lung_volume_fraction*pbpk.bw*(1-pbpk.blood_lung_fraction); %L
pbpk.kidney_volume=pbpk.kidney_volume_fraction*pbpk.bw*(1-pbpk.blood_kidney_fraction); %L
pbpk.bladder_volume=pbpk.bladder_volume_fraction*pbpk.bw*(1-pbpk.blood_bladder_fraction); %L
pbpk.skin_volume=pbpk.skin_volume_fraction*pbpk.bw*(1-pbpk.blood_skin_fraction); %L
pbpk.rest_volume=pbpk.rest_volume_fraction*pbpk.bw*(1-pbpk.blood_rest_fraction); %L
pbpk.blood_volume=pbpk.blood_volume_fraction*pbpk.bw; %L

%Absolute Values of Volumes of Blood in various compartments (in L):
pbpk.blood_lung=pbpk.blood_lung_fraction*pbpk.lung_volume_fraction*pbpk.bw;%L
pbpk.blood_skin=pbpk.blood_skin_fraction*pbpk.skin_volume_fraction*pbpk.bw;%L
pbpk.blood_liver=pbpk.blood_liver_fraction*pbpk.liver_volume_fraction*pbpk.bw;%L
pbpk.blood_kidney=pbpk.blood_kidney_fraction*pbpk.kidney_volume_fraction*pbpk.bw;%L
pbpk.blood_bladder=pbpk.blood_bladder_fraction*pbpk.bladder_volume_fraction*pbpk.bw;%L
pbpk.blood_rest=pbpk.blood_rest_fraction*pbpk.rest_volume_fraction*pbpk.bw;%L

pbpk.plasma_volume = (1-pbpk.hematocrit)*pbpk.blood_volume;
pbpk.rbc_volume = pbpk.hematocrit*pbpk.blood_volume;
%% Partition coefficients
% All values here are unitless
pbpk.P_lung=0.44;
pbpk.P_liver=1.6;
pbpk.P_kidney=0.14;
pbpk.P_skin=1.6;
pbpk.P_bladder=1.6;
pbpk.P_rest=1.6;

%% Kinetic Constants
pbpk.k_met=0.07;%1/hr Metabolic clearance in the liver
pbpk.k_bile=0;
pbpk.k_kindey=0.0164;%1/hr Kidney Clearance

%% Permeability Coefficients
% All in 1/hr
pbpk.pi_lung=0.94;
pbpk.pi_liver=0.0095;
pbpk.pi_kidney=0.12;
pbpk.pi_skin=0.0095;
pbpk.pi_bladder=0.0095;
pbpk.pi_rest=0.0095;
pbpk.pi_rbc=1.3;%also called 'binding constant for RBCs'
pbpk.pi_plasma=0.81;%also called 'Plasma Binding Constant'

%% Constraints
pbpk.max_liver=1.4e-6;
pbpk.max_kidney=5e-6;%5e-4
pbpk.max_influx=0.04e-6;%g/hr