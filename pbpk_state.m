function SS=pbpk_state(pbpk)
%PBPK_STATE returns a state space representation of the LTI systems that
%comprise the given PBPK model.

%{
    Offset-free Model Predictive Control for Optimal Drug Administration
    Copyright (C) 2012-2014 Pantelis Sopasakis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

%% Skin: 1Input, 2States, 2Outputs
% x1: C_v_skin
% x2: C_skin
% u : C_art
SS.skin.A=[ [ -pbpk.pi_skin-pbpk.skin_flow   pbpk.pi_skin/pbpk.P_skin]/pbpk.blood_skin
    [pbpk.pi_skin                  -pbpk.pi_skin/pbpk.P_skin ]/pbpk.skin_volume
    ];
SS.skin.B=[ pbpk.skin_flow/pbpk.blood_skin  ; 0];
SS.skin.C=[1 0];
SS.skin.D=0;
SS.skin.csys=ss(SS.skin.A,SS.skin.B,SS.skin.C,SS.skin.D,'inputname','Cart',...
    'statename',{'Cvs','Cs'},'outputname','Cvs');

%% Kidney: 1Input, 2States, 2Outputs
% x1: Cvkid
% x2: Ckid
% u : Cart
SS.kidney.A=[
    [-pbpk.kidney_flow-pbpk.pi_kidney-pbpk.k_kindey*pbpk.blood_kidney   pbpk.pi_kidney/pbpk.P_kidney]/pbpk.blood_kidney
    [pbpk.pi_kidney           -pbpk.pi_kidney/pbpk.P_kidney]/pbpk.kidney_volume
    ];
SS.kidney.B=[pbpk.kidney_flow/pbpk.blood_kidney ; 0];
SS.kidney.C=[1 0];
SS.kidney.D=0;
SS.kidney.csys=ss(SS.kidney.A,SS.kidney.B,SS.kidney.C,SS.kidney.D,'inputname','Cart',...
    'statename',{'Cvk','Ck'},'outputname','Cvk');

%% Bladder: 1Input, 2States, 2Outputs
% x1: Cvbladder
% x2: Cbladder
% u : Cart
SS.bladder.A=[
    [-pbpk.pi_bladder-pbpk.bladder_flow    pbpk.pi_bladder/pbpk.P_bladder]/pbpk.blood_bladder
    [pbpk.pi_bladder      -pbpk.pi_bladder/pbpk.P_bladder]/pbpk.bladder_volume
    ];
SS.bladder.B=[pbpk.bladder_flow/pbpk.blood_bladder ; 0];
SS.bladder.C=[1 0];
SS.bladder.D=0;
SS.bladder.csys=ss(SS.bladder.A,SS.bladder.B,SS.bladder.C,SS.bladder.D,'inputname','Cart',...
    'statename',{'Cvbl','Cbl'},'outputname','Cvbl');

%% Residual: 1Input, 2States, 2Outputs
% x1: Cvr
% x2: Cr
% u : Cart
SS.rest.A=[
    [-pbpk.pi_rest-pbpk.rest_flow    pbpk.pi_rest/pbpk.P_rest]/pbpk.blood_rest
    [pbpk.pi_rest      -pbpk.pi_rest/pbpk.P_rest]/pbpk.rest_volume
    ];
SS.rest.B=[pbpk.rest_flow/pbpk.blood_rest ; 0];
SS.rest.C=[1 0];
SS.rest.D=0;
SS.rest.csys=ss(SS.rest.A,SS.rest.B,SS.rest.C,SS.rest.D,'inputname','Cart',...
    'statename',{'Cvr','Cr'},'outputname','Cvr');

%% Liver: 1Input, 2States, 2Outputs
% x1: Cvliv
% x2: Cliv
% u : Cart
SS.liver.A=[
    [-pbpk.pi_liver-pbpk.liver_flow    pbpk.pi_liver/pbpk.P_liver]/pbpk.blood_liver
    [pbpk.pi_liver  -pbpk.pi_liver/pbpk.P_liver-(pbpk.k_met+pbpk.k_bile)*pbpk.liver_volume]/pbpk.liver_volume
    ];
SS.liver.B=[pbpk.liver_flow/pbpk.blood_liver ; 0];
SS.liver.C=[1 0];
SS.liver.D=0;
SS.liver.csys=ss(SS.liver.A,SS.liver.B,SS.liver.C,SS.liver.D,'inputname','Cart',...
    'statename',{'Cvliv','Cliv'},'outputname','Cvliv');

%% Blood: 6Inputs, 2Stats, 2Outputs
% u1: Cvs
% u2: Cvk
% u3: Cvb (bladder)
% u4: Cvr
% u5: Cvliv
% u6: u (ivrate)
% x1: Cplasma
% x2: Crbc
SS.blood.A=[
    [-pbpk.pi_plasma-pbpk.cardiac_output   pbpk.pi_rbc]/pbpk.plasma_volume
    [pbpk.pi_plasma                       -pbpk.pi_rbc]/pbpk.rbc_volume
    ];
SS.blood.B=[
    [pbpk.skin_flow pbpk.kidney_flow pbpk.bladder_flow pbpk.rest_flow pbpk.liver_flow 1]/pbpk.plasma_volume
    zeros(1,6)];
SS.blood.C=[1 0];
SS.blood.D=zeros(1,6);
SS.blood.csys=ss(SS.blood.A,SS.blood.B,SS.blood.C,SS.blood.D,'inputname',...
    {'Cvs','Cvk','Cvbl','Cvr','Cvliv','u'},...
    'statename',{'Cpl','Crbc'},'outputname','Cpl');

%% Lung: 1Input, 2States, 2Outputs
% u : Cpl
% x1: Cart
% x2: Clu
SS.lung.A = [
    [-pbpk.pi_lung-pbpk.cardiac_output  pbpk.pi_lung/pbpk.P_lung]/pbpk.blood_lung
    [pbpk.pi_lung -pbpk.pi_lung/pbpk.P_lung]/pbpk.lung_volume
    ];
SS.lung.B=[pbpk.cardiac_output/pbpk.blood_lung ; 0];
SS.lung.C=[1 0];
SS.lung.D=0;
SS.lung.csys=ss(SS.lung.A,SS.lung.B,SS.lung.C,SS.lung.D,'inputname','Cpl',...
    'statename',{'Cart','Clu'},'outputname','Cart');

SS.overall=...
    connect(SS.blood.csys,...
    SS.lung.csys,...
    SS.skin.csys,...
    SS.bladder.csys,...
    SS.liver.csys,...
    SS.rest.csys,...
    SS.kidney.csys,...
    'u','Cpl');

% %% Slow and Fast Manifolds
% SS.M=4e3;
% SS.epsilon=1/SS.M;
% n=length(SS.overall.a);
% hasHugeNum=sum(abs(SS.overall.a')>SS.M)'>0;
% numHuge=sum(hasHugeNum);
% linesWithHugeNum=zeros(1,numHuge);
% normalLines=1:n;
% j=1;
% for i=1:n    
%     if (hasHugeNum(i)==1)
%         linesWithHugeNum(1,j)=i;
%         j=j+1;
%     end
% end
% normalLines(linesWithHugeNum)=[];
% SS.overall=xperm(SS.overall,[normalLines linesWithHugeNum]);
% 
% % Construction of the perturbed system:
% % x'  = A11 x + A12 z + B1 u
% % εz' = A21 x + A22 z + B2 u
% A11=SS.overall.a(normalLines,normalLines);
% A12=SS.overall.a(normalLines,linesWithHugeNum);
% A21=SS.overall.a(linesWithHugeNum,normalLines)/SS.M;
% A22=SS.overall.a(linesWithHugeNum,linesWithHugeNum)/SS.M;
% B1=SS.overall.b(normalLines);
% B2=SS.overall.b(linesWithHugeNum);
% 
% [L H]=changMatrices(A11,A12,A21,A22,SS.epsilon);
% 
% % Define the matrix T s.t. [σ φ]=T*[x z]
% T=[eye(n-numHuge)-SS.epsilon*H*L  -SS.epsilon*H
%    L                                eye(numHuge)];
% 
% % Define the slow-fast systems:
% % σ' = (A11-A12*L)*σ     + T*B1*u
% % φ' = (Α22 + ε*L*A12)*φ + T*B2*u
