function [L, H, Lerror, Herror]=changMatrices(A11,A12,A21,A22,epsilon,maxIterations)

%{
    Offset-free Model Predictive Control for Optimal Drug Administration
    Copyright (C) 2012-2014 Pantelis Sopasakis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

if nargin<=5
    maxIterations=10;
end
L=A22\A21;
Lerror=-1;
for i=1:maxIterations
    Lnew = A22\(  A21 +  epsilon*L*(A11-A12*L) );
    Lerror=norm(L-Lnew);
    if (Lerror<1e-4)
        break;
    end
    L=Lnew;
end
Herror=-1;
H=A12/A22;
for i=1:maxIterations
    Hnew = (A12 - epsilon*(H*L*A12-A11*H + A12*L*H)  )/A22;
    Herror=norm(H-Hnew);
    if (Herror<1e-4)
        break;
    end
    H=Hnew;
end


