function [SS2, pbpk2] = pbpk_perturb(pbpk, f)
%PBPK perturb creates a randomly perturbed variant of a pbpk object according to 
%a perturbation parameter. The perturbed pbpk structure and its state space
%representation are returned. 
%
%Syntax:
%[SS2, pbpk2] = pbpk_perturb(pbpk, f)
%
%Input arguments:
%pbpk  - A PBPK structure such as the one returned by pbpk_init
%f     - A perturbation parameter. All the parameters of the model are
%        multiplied by a factor in the form (1+r*sqrt(f)) where r is a random
%        number which follows the normal distribution with zero mean and
%        variance equal to 1 and f is the perturbaction factor provided by
%        the user. The term (1+r*sqrt(f)) follows the distribution N(1, f)
%

%Author:
%Pantelis Sopasakis

%{
    Offset-free Model Predictive Control for Optimal Drug Administration
    Copyright (C) 2012-2014 Pantelis Sopasakis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

f=sqrt(f);
if (abs(f)<1e-6)
    pbpk2=pbpk;
    SS2=pbpk_state(pbpk2);
else
    pbpk2=pbpk;
        
    pbpk2.cardiac_output=pbpk.cardiac_output*(1+randn*f);
    pbpk2.liver_flow_factor=pbpk.liver_flow_factor*(1+randn*f);
    pbpk.kidney_flow_factor=pbpk.kidney_flow_factor*(1+randn*f);
    pbpk.skin_flow_factor=pbpk.skin_flow_factor*(1+randn*f);
    pbpk.bladder_flow_factor=pbpk.bladder_flow_factor*(1+randn*f);
    pbpk2.rest_flow_factor= 1 - pbpk2.liver_flow_factor -pbpk2.kidney_flow_factor -...
        pbpk2.skin_flow_factor - pbpk2.bladder_flow_factor;    
    pbpk2.liver_flow=pbpk2.liver_flow_factor * pbpk2.cardiac_output;%L/hr
    pbpk2.kidney_flow=pbpk2.kidney_flow_factor * pbpk2.cardiac_output;%L/hr
    pbpk2.skin_flow=pbpk2.skin_flow_factor * pbpk2.cardiac_output;%L/hr
    pbpk2.bladder_flow=pbpk2.bladder_flow_factor * pbpk2.cardiac_output;%L/hr
    pbpk2.lung_flow=pbpk2.lung_flow_factor * pbpk2.cardiac_output;%L/hr
    pbpk2.rest_flow=pbpk2.rest_flow_factor * pbpk2.cardiac_output;%L/hr
    
   
    pbpk2.liver_volume_fraction=pbpk.liver_volume_fraction*(1+randn*f);%L/kg
    pbpk2.kidney_volume_fraction=pbpk.kidney_volume_fraction*(1+randn*f);
    pbpk2.skin_volume_fraction=pbpk.skin_volume_fraction*(1+randn*f);
    pbpk2.bladder_volume_fraction=pbpk.bladder_volume_fraction*(1+randn*f);
    pbpk2.lung_volume_fraction=pbpk.lung_volume_fraction*(1+randn*f);
    pbpk2.rest_volume_fraction = 1 - pbpk2.liver_volume_fraction - ...
        pbpk2.kidney_volume_fraction -...
        pbpk2.skin_volume_fraction - ...
        pbpk2.bladder_volume_fraction - ...
        pbpk2.lung_volume_fraction;
    pbpk.blood_volume_fraction=50/100;
    
    % Dimensionells fractions of volume:
    pbpk2.blood_lung_fraction=pbpk.blood_lung_fraction*(1+randn*f);
    pbpk2.blood_skin_fraction=pbpk.blood_skin_fraction*(1+randn*f);
    pbpk2.blood_liver_fraction=pbpk.blood_liver_fraction*(1+randn*f);
    pbpk2.blood_kidney_fraction=pbpk.blood_kidney_fraction*(1+randn*f);
    pbpk2.blood_bladder_fraction=pbpk.blood_bladder_fraction*(1+randn*f);
    pbpk2.blood_rest_fraction=pbpk.blood_rest_fraction*(1+randn*f);
    
    
    
    pbpk2.liver_volume=pbpk2.liver_volume_fraction*pbpk2.bw*(1-pbpk2.blood_liver_fraction); %L
    pbpk2.lung_volume=pbpk2.lung_volume_fraction*pbpk2.bw*(1-pbpk2.blood_lung_fraction); %L
    pbpk2.kidney_volume=pbpk2.kidney_volume_fraction*pbpk2.bw*(1-pbpk2.blood_kidney_fraction); %L
    pbpk2.bladder_volume=pbpk2.bladder_volume_fraction*pbpk2.bw*(1-pbpk2.blood_bladder_fraction); %L
    pbpk2.skin_volume=pbpk2.skin_volume_fraction*pbpk2.bw*(1-pbpk2.blood_skin_fraction); %L
    pbpk2.rest_volume=pbpk2.rest_volume_fraction*pbpk2.bw*(1-pbpk2.blood_rest_fraction); %L
    pbpk2.blood_volume=pbpk2.blood_volume_fraction*pbpk2.bw; %L
    
    %Absolute Values of Volumes of Blood in various compartments (in L):
    pbpk2.blood_lung=pbpk2.blood_lung_fraction*pbpk2.lung_volume_fraction*pbpk2.bw;%L
    pbpk2.blood_skin=pbpk2.blood_skin_fraction*pbpk2.skin_volume_fraction*pbpk2.bw;%L
    pbpk2.blood_liver=pbpk2.blood_liver_fraction*pbpk2.liver_volume_fraction*pbpk2.bw;%L
    pbpk2.blood_kidney=pbpk2.blood_kidney_fraction*pbpk2.kidney_volume_fraction*pbpk2.bw;%L
    pbpk2.blood_bladder=pbpk2.blood_bladder_fraction*pbpk2.bladder_volume_fraction*pbpk2.bw;%L
    pbpk2.blood_rest=pbpk2.blood_rest_fraction*pbpk2.rest_volume_fraction*pbpk2.bw;%L
    
    pbpk2.plasma_volume = (1-pbpk2.hematocrit)*pbpk2.blood_volume;
    pbpk2.rbc_volume = pbpk2.hematocrit*pbpk2.blood_volume;
    %% Partition coefficients
    % All values here are unitless
    pbpk2.P_lung=0.44*(1+randn*f);
    pbpk2.P_liver=1.6*(1+randn*f);
    pbpk2.P_kidney=0.14*(1+randn*f);
    pbpk2.P_skin=1.6*(1+randn*f);
    pbpk2.P_bladder=1.6*(1+randn*f);
    pbpk2.P_rest=1.6*(1+randn*f);
    
    %% Kinetic Constants
    pbpk2.k_met=0.07*(1+randn*f);%1/hr Metabolic clearance in the liver
    pbpk2.k_bile=0;
    pbpk2.k_kindey=0.0164*(1+randn*f);%1/hr Kidney Clearance
    
    %% Permeability Coefficients
    % All in 1/hr
    pbpk2.pi_lung=0.94*(1+randn*f);
    pbpk2.pi_liver=0.0095*(1+randn*f);
    pbpk2.pi_kidney=0.12*(1+randn*f);
    pbpk2.pi_skin=0.0095*(1+randn*f);
    pbpk2.pi_bladder=0.0095*(1+randn*f);
    pbpk2.pi_rest=0.0095*(1+randn*f);
    pbpk2.pi_rbc=1.3*(1+randn*f);%also called 'binding constant for RBCs'
    pbpk2.pi_plasma=0.81*(1+randn*f);%also called 'Plasma Binding Constant'
    
    %% Constraints
    pbpk2.max_liver=1.4e-6*(1+randn*f);
    pbpk2.max_kidney=5e-6*(1+randn*f);%5e-4
    pbpk2.max_influx=0.15e-6*(1+randn*f);%ug/hr
    SS2=pbpk_state(pbpk2);
end

