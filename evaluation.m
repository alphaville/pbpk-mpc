%EVALUATION is a script used to perform Monte-Carlo-type simulations to
%model the intra-patient variability and the effect the measurement noise
%on the behaviour of the closed loop in presence of the proposed MPC
%controller.
%
%See also
%PBPK
%

%Author: 
%Pantelis Sopasakis

%{
    Offset-free Model Predictive Control for Optimal Drug Administration
    Copyright (C) 2012-2014 Pantelis Sopasakis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

clear all
clear global 

set(0,'DefaultAxesFontName', 'Helvetica')
set(0,'DefaultAxesFontSize', 14)
set(0,'DefaultTextFontname', 'Helvetica')
set(0,'DefaultTextFontSize', 14)

%% MAP
TS_KEYWORDS = {'Liver','Kidney','Plasma','Skin','Bladder','Lung','Residual','RBC'};
MAPPING_INDEX = [10, 14, 1, 6, 8, 4, 12, 3];

%% MC SIMULATIONS
eval_filename = ['./eval-data/eval-' strrep(strrep(datestr(clock),' ','-'),':','-') '.txt'];
fid = fopen(eval_filename,'w');
Nfold = 50;
OD_Liver=zeros(Nfold,1);
OD_Kidney=zeros(Nfold,1);
RMSOD_Liver=zeros(Nfold,1);
Max_Liver=zeros(Nfold,1);
T_OD_Liver=zeros(Nfold,1);
COMPUTATIONAL_TIMES = zeros(Nfold,1);


for i=1:Nfold
    tic;
    disp(['>>>>> Fold: ', num2str(i) '/' num2str(Nfold)]);
    PBPK;
    sim('pbpk_control','');
    cont_resp = pbpk_resp.signals.values;
    P=size(cont_resp,1);
    if (i==1)        
        Conc_Liver=zeros(P,Nfold);
        Conc_Kidney=zeros(P,Nfold);
        Conc_Plasma=zeros(P,Nfold);
        Conc_skin=zeros(P,Nfold);
        Conc_bladder=zeros(P,Nfold);
        Conc_residual=zeros(P,Nfold);
        Conc_lung=zeros(P,Nfold);
        Conc_rbc=zeros(P,Nfold);
    end
    time = pbpk_resp.time;
    dt = time(2)-time(1);
    T=length(cont_resp);
    overdose_liver = 0;
    overdose_kidney = 0;
    rmsod_liver=0;
    t_od_liver=0;
    for t=1:T
        overdose_liver= overdose_liver + max([0 cont_resp(t,10)-pbpk.max_liver])*dt;
        if (cont_resp(t,10)>pbpk.max_liver)
            t_od_liver = t_od_liver+dt;
        end
        overdose_kidney= overdose_kidney+ max([0 cont_resp(t,14)-pbpk.max_kidney])*dt;
        rmsod_liver = rmsod_liver + max([0 cont_resp(t,10)-pbpk.max_liver])^2*dt;
    end
    % Retrieve the various drug concentration data (per tissue)
    for j = 1:length(TS_KEYWORDS)
        eval(['Conc_' TS_KEYWORDS{j} '(1:P,i)=cont_resp(:,' num2str(MAPPING_INDEX(j)) ');'])
    end    
    
    max_liver=max(cont_resp(:,10));
    OD_Kidney(i)=overdose_kidney;
    OD_Liver(i)=overdose_liver;
    Max_Liver(i)=max_liver;
    RMSOD_Liver(i)=sqrt(rmsod_liver);
    elapsed_time = toc;
    COMPUTATIONAL_TIMES(i)=elapsed_time;
    average_computational_time = mean(COMPUTATIONAL_TIMES(max([1,i-5]):i));
    estimated_remaining_time = average_computational_time*(Nfold-i);
    % Log messages:
    fprintf(fid,'%s\n',['>>>>> Fold            : ', num2str(i)]);    
    fprintf(fid,'%s\n',['<<<<< Overdose Liver  : ', num2str(overdose_liver*1e6), 'ug']);    
    fprintf(fid,'%s\n',['<<<<< Overdose Kidney : ', num2str(overdose_kidney*1e6), 'ug']);
    fprintf(fid,'%s\n',['<<<<< RMSOD Liver     : ', num2str(sqrt(rmsod_liver)*1e6), 'ug']);
    fprintf(fid,'%s\n',['<<<<< Max Concentr.   : ', num2str(max_liver*1e6), 'ug']);
    fprintf(fid,'%s\n',['<<<<< Time Overdose   : ', num2str(t_od_liver), 'min']);
    fprintf(fid,'%s\n',['----- Elapsed Time    : ', num2str(elapsed_time), 'sec']);
    fprintf(fid,'%s\n',' ');
    disp(['<<<<< Current Time               : ' num2str(elapsed_time) 's']);
    disp(['<<<<< Remaining Time (Estimated) : ' num2str(estimated_remaining_time/60) 'min']);
    pause(1)
end
R=struct;
R.OD_Liver = OD_Liver;
R.OD_Kidney=OD_Kidney;
R.Max_Liver=Max_Liver;
R.i=i;
R.Conc_Liver=Conc_Liver;
R.Conc_Kidney=Conc_Kidney;
R.Conc_Plasma=Conc_Plasma;
R.RMSOD_Liver=RMSOD_Liver;
%save('eval.mat','-struct','R');
save eval
fclose(fid);

nTimePoints=length(pbpk_resp.time)-5;
%% COMPUTATION OF STATISTICAL PARAMETERS
disp('<<<<< Computing MC statistics');
num_std = 2;
conf_int=90;
for i_keyword=1:length(TS_KEYWORDS)
    tissue_keyword=TS_KEYWORDS{i_keyword};
    eval(['C' tissue_keyword '_ave = zeros(nTimePoints,1);']);
    eval(['C' tissue_keyword '_up = zeros(nTimePoints,1);'])
    eval(['C' tissue_keyword '_low = zeros(nTimePoints,1);'])
    eval(['C' tissue_keyword '_std = zeros(nTimePoints,1);'])
    for i=1:nTimePoints
        eval(['C' tissue_keyword '_ave(i) = mean(Conc_' tissue_keyword '(i,1:Nfold));']);
        eval(['C' tissue_keyword '_std(i) = std(Conc_' tissue_keyword '(i,1:Nfold));']);
        eval(['C' tissue_keyword '_up(i) = prctile(Conc_' tissue_keyword '(i,1:Nfold), ' num2str(conf_int) ', 2);']);
        eval(['C' tissue_keyword '_low(i) = prctile(Conc_' tissue_keyword '(i,1:Nfold), ' num2str(100-conf_int) ', 2);']);
    end
end

%% PLOTS 
disp('>>>>> Plotting');
figure(32);
set(0,'DefaultAxesFontName', 'Arial')
set(0,'DefaultAxesFontSize', 12)

for i_plot = 1:length(TS_KEYWORDS)
    subplot(2,4,i_plot)
    hold on;
    for i=1:Nfold
        eval(['plot(pbpk_resp.time(1:nTimePoints),Conc_' TS_KEYWORDS{i_plot} '(1:nTimePoints,i)*1e6,''y'')'])
    end
    %ylabel('Plasma Concentration (ug/L)');
    eval(['plot(pbpk_resp.time([1:nTimePoints]),C' TS_KEYWORDS{i_plot} '_ave*1e6,''b'',''LineWidth'',1);'])
    eval(['plot(pbpk_resp.time([1:nTimePoints]),C' TS_KEYWORDS{i_plot} '_low*1e6,''m --'',''LineWidth'',1)']);
    eval(['plot(pbpk_resp.time([1:nTimePoints]),C' TS_KEYWORDS{i_plot} '_up*1e6,''m --'',''LineWidth'',1)']);
    if i_plot==3,
        plot(c_pl.time,c_pl.signals.values(:,1)*1e6,'k --');%reference
    end   
    title(TS_KEYWORDS{i_plot})
    xlabel('Time (hr)');
    ylabel('Concentration (ug/L)')
    grid on;
    axis tight;
end