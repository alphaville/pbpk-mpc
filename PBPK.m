%PBPK is a script used to initialize all necessary parameters related to
%the PBPK model described by Evans et al. It uses pbpk_init to initialize
%the pharmocokinetic parameters and then casts the dynamical system as a
%continuous-time LTI system which is discretized and augmented by a
%disturbance model. A Kalman observer is then developed for the augmented
%system which is used within the formulation of an MPC problem.
%
%After running PBPK, run the simulink file pbpk_control to perform the
%closed-loop simulations.
%
%See Also:
%pbpk_control, pbpk_init
%

%Author:
%Pantelis Sopasakis



%{
    Offset-free Model Predictive Control for Optimal Drug Administration
    Copyright (C) 2012-2014 Pantelis Sopasakis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

%% Initialisation
global mpcmatrices Bd Cd SS2 dsys pbpk
yalmip clear

sdpsettings('solver','cplex'); % QPC is quite fast - it needs to be in the path
pbpk = pbpk_init(0.030,0.45);   % Constants [Body Weight, Hematocrit]
                              % See: help pbpk_init
SS = pbpk_state(pbpk);          % The state state system(s)

pbpk_measurement_noise = 0;%.5e-7;  % Measurement noise factor (default: 1e-7)

%SS2 = pbpk_perturb(pbpk, 0.003);  % Default: 0.02 (2%)
SS2 = SS;

target1=0.4E-6;
target2=0.8E-6;

% Discretization:
Ts=5/60; % 5min
dsys=c2d(SS.overall,Ts,'zoh');
n=size(dsys.a,1);
m=size(dsys.b,2);
p=size(dsys.c,1);

% Check whether (A,C) is detectable [Discrete-time System]
OBS = obsv(dsys.a, dsys.c);
SV=svd(OBS);
flagObsv = sum(~SV>0);
if (flagObsv>0)
    error('(A,C) is not observable');
end
    
%% Construct the Augmented System

% Construct the augmented system 
% (Specify Bd and Cd):
Bd=[1;0;0;0;1;0;3;2;0;1;0;0;0;1];
Cd=1;


% Construct the augmented system:
A_=[dsys.a    Bd
   zeros(1,size(dsys.a,2)) 1];
B_=[dsys.b 
    0];
C_ = [dsys.C  Cd];
D_ = 0;

% Check whether the observability condition is satisfied:
XX=[eye(size(dsys.a,1))-dsys.a     -Bd
    dsys.c                          Cd];
SVX=svd(XX);
flagX = sum(~SVX>0);
if (flagObsv>0)
    error('Xi is not observable');
end

%% Kalman Observer Design
G1=ones(n+p,1);
G1(1)=10;
G1(10)=10;
G2=10;
QN=5;  RN=20;  NN=1;
syskal=dss(A_, [B_ G1], C_, [D_ G2],eye(15),Ts);
[~,L] = kalman(syskal,QN,RN,NN);
Lx=L(1:n,1);
Ld=L(n+p,1);

kalA=[dsys.a-Lx*dsys.c Bd-Lx*Cd; -Ld*dsys.c 1-Ld*Cd];
kalB=[dsys.b Lx;0 Ld];
kalC=eye(n+p);
kalD=zeros(n+p,2);
aug_observer = struct('kalA', kalA, 'kalB', kalB, 'kalC', kalC, 'kalD', kalD);

%% MPC Matrices
%Define matrices needed for MPC (Design parameters):
N=35;
Q=eye(n)/4;
Q(10,10)=1000;
Q(1,1)=40;
Q=55000*Q;
R=5*eye(m);
mpcmatrices = mpcmat(dsys.a,dsys.b,Bd,N,Q,R);
mpcmatrices.nx = n;
mpcmatrices.nu = m;

%% Plot results
%sim('pbpk_control');
%plotter
