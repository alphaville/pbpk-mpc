function [x_,u_]=tracking(dsys, Bd, Cd, d_hat, r)
%TRACKING returns the values of x_, u_ used within the offset-free MPC
%problem.
%
%Input arguments:
% dsys   : The discrete-time LTI system
% Bd, Cd : The matrices used to formulate the augmented-state system using a
%          disturbance model.
% d_hat  : The current estimated disturbace
% r      : The reference input to the controller
%
%Output:
%x_, u_  : The tracking parameters needed in MPC.
%
%See also:
%mpcmat, getInput, controller

%{
    Offset-free Model Predictive Control for Optimal Drug Administration
    Copyright (C) 2012-2014 Pantelis Sopasakis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

L= [dsys.a-eye(size(dsys.a))   dsys.b
    dsys.c                     zeros(size(dsys.c,1),size(dsys.b,2))];
R= [-Bd*d_hat
    r-Cd*d_hat];
xu=L\R;
x_=xu(1:size(dsys.a,1));
u_=xu(size(dsys.a,1)+1:end);