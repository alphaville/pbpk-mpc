function u = controller(inp)
%CONTROLLER is simply a wrapper for the function getInput
%
%Syntax:
% u = controller(inp)
%
%Input arguments:
% inp: The vector inp = [x' d' r']', where x is the state of the system, d
% is the disturbance and r is the reference signal (here is is assumed to
% be a scalar).
%
%Output arguments:
% u: The control action calculated by the offset-free MPC algorithm.
%
%See also:
%getInput
%

%{
    Offset-free Model Predictive Control for Optimal Drug Administration
    Copyright (C) 2012-2014 Pantelis Sopasakis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}
global mpcmatrices Bd Cd dsys
n=mpcmatrices.nx;
x = inp(1:n);
d = inp(n+1:end-1);
r = inp(end);
[x_,u_] = tracking(dsys,Bd,Cd,d,r);
u = getInput(x, d, x_, u_);
end