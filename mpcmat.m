function mpcmatrices = mpcmat(A,B,Bd,N,Q,R)
%MPCMAT returns the matrices Sx, Sd and Su so that the state evolution of
%an LTI system is described by Y=Sx*x0+Sd*d+Su*U for the discrete-time LTI
%system x+=A*x+B*u+Bd*d; where x, u and d stand for the state, input and
%disturbance variables respectively. The above relation is written as:
%
%     [ x0 ]              [ u0   ]
%     [ x1 ]              [ u1   ]
% Y = [    ] = Sx*x0 + Su*[      ] + Sd*d
%     [    ]              [      ]
%     [ xN ]              [ uN-1 ]
%                            
%Additionally, Q and R are the weight-matrices of a quadratic cost
%function of the form: V(x0,U)=xN'*P*xN + Σ_k=0...N-1 xk'Qxk + uk'Quk.
%This cost function is then written in compact form as V(Y,U)=Y'Q_Y+U'R_U
%where Y and U as above and Q_ and R_ are calculated in this function. The
%matrix P is also calculated so that it satisfies an algebraic Ricatti
%equation.
%
%The matrices Q_ and R_ are given by the following formulas:
%
%       [Q           ]          
%       [  Q         ]          [ R      ]
%  Q_ = [    .       ] and R_ = [   .    ] = diag(R,R,...,R)
%       [       Q    ]          [      R ]
%       [          P ]
%
%Input:
% A,B,Bd Matrices of the LTI system x+=A*x+B*u+Bd*d
% N Simulation horizon
% Q,R Weight matrices of the quadratic cost function 
%
%Output:
% Sx,Su,Sd: Constants of the equation Y=Sx*x0+Sd*d+Su*U.
% P: Lyapunov matrix calculated using an algebraic Ricatti equation
% Q_,R_: Weight matrices used in the cost function V(Y,U) = Y' Q_ Y+ U' R_ U
%
%Sizes:
% Let x be an n-vector, u an m-vector and d an s-vector. Then A is a n-by-n
% matrix, B is an n-by-m matrix and Bd is an n-by-s matrix. Let N be the
% simulation horizon. Then Sx is a matrix of size n(N+1)-by-n, Su has size
% n(N+1)-by-mN and Sd is n(N+1)-by-s. 
%
%See Also:
%getInput, controller, tracking
%
%Author: 
%Pantelis Sopasakis

%{
    Offset-free Model Predictive Control for Optimal Drug Administration
    Copyright (C) 2012-2014 Pantelis Sopasakis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

%sizes
n=size(A,1);
m=size(B,2);
s=size(Bd,2);

% Calculation of Sx
Sx=zeros(n*(N+1),n);
Sx(1:n,:)=eye(n);
for i=2:N+1
    Sx(1+(i-1)*n:i*n,:)=Sx(1+(i-2)*n:(i-1)*n,:)*A;
end

% Calculation of Su
Su=zeros(n*(N+1),m*N);
for j=1:N,
    Ac=eye(n);
    for k=j:-1:1,
        Su(n+1+n*(j-1):n+n*j,1+m*(k-1):k*m)=Ac*B;
        Ac=Ac*A;
    end
end

%Calculation of Sd
Sd=zeros(n*(N+1),s);
Sd(n+1:2*n)=Bd;
temp=eye(n);
power=eye(n);
for j=1:N
    Sd(1+j*n:(1+j)*n,:)=temp*Bd;
    power=power*A;
    temp = temp + power;
end

[~,P]=dlqr(A, B, Q, R, 0); %Lyapunov matrix

Q_=blkdiag(kron(eye(N),Q),P);
R_=kron(eye(N),R);
mpcmatrices.Sx=Sx;
mpcmatrices.Su=Su;
mpcmatrices.Sd=Sd;
mpcmatrices.P=P;
mpcmatrices.Q=Q;
mpcmatrices.R=R;
mpcmatrices.Q_=Q_;
mpcmatrices.R_=R_;
mpcmatrices.N=N;
end