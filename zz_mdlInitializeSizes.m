function [sys,x0,str,Tvector]=zz_mdlInitializeSizes(nInput,nStates,nOutput)
% Modify only the initial condition of states
sizes=simsizes;
sizes.NumContStates=nStates;  % Number of states
sizes.NumDiscStates=0;
sizes.NumOutputs=nOutput;     % Number of Outputs
sizes.NumInputs=nInput;      % Number of Inputs automatically caLculated by MATLAB
sizes.DirFeedthrough=0;
sizes.NumSampleTimes=1;
sys=simsizes(sizes);
x0=[0 0];
str=[];
Tvector=[0 0];
