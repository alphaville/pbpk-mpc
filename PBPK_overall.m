%% State Space
% The State Vector (n=14):
% C = [Cpl          1
%      Crbc         2
%      C_v_skin     3
%      C_skin       4
%      C_v_liver    5
%      C_liver      6
%      C_v_kidney   7
%      C_kidney     8
%      C_v_bladder  9
%      Cbladder     10
%      C_v_residual 11
%      C_residual   12
%      C_art        13 
%      C_lung    ]  14 
% Note: We divide by the volumes in the end...

%{
    Offset-free Model Predictive Control for Optimal Drug Administration
    Copyright (C) 2012-2014 Pantelis Sopasakis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

A = zeros(14,14);
B = [1; zeros(13,1)];
C = [1 zeros(1,13)];
D = 0;

%% eqn::Blood [1,2]
% a. plasma
A(1,:) = [-pbpk.pi_plasma-pbpk.cardiac_output    pbpk.pi_rbc ...
    pbpk.skin_flow 0 ...
    pbpk.liver_flow 0 ...
    pbpk.kidney_flow 0 ...
    pbpk.bladder_flow 0 ... 
    pbpk.rest_flow zeros(1,3)]/pbpk.plasma_volume;

% b. RBCs
A(2,1:2) = [pbpk.pi_plasma -pbpk.pi_rbc]/pbpk.rbc_volume;
%% eqn::Skin [3,4]

% a. skin blood
A(3, [3 4 13])=[-pbpk.pi_skin-pbpk.skin_flow ...
    pbpk.pi_skin/pbpk.P_skin ...
    pbpk.skin_flow]/pbpk.blood_skin;

% b. skin tissue
A(4,3:4)=pbpk.pi_skin*[1 -1/pbpk.P_skin]/pbpk.skin_volume;
%% eqn::Liver [5,6]

% a. liver blood
A(5,[5 6 13])=[-pbpk.pi_liver-pbpk.liver_flow ...
    pbpk.pi_liver/pbpk.P_liver ...
    pbpk.liver_flow]/pbpk.blood_liver;

% b. liver tissue
A(6,5:6)=[pbpk.pi_liver ...
    -pbpk.pi_liver/pbpk.P_liver-(pbpk.k_met+pbpk.k_bile)*pbpk.liver_volume]...
    /pbpk.liver_volume;
%% eqn::Kidney [7,8]

% a. kidney blood
A(7,[7 8 13])=[-pbpk.kidney_flow-pbpk.pi_kidney-pbpk.k_kindey*pbpk.blood_kidney ...
    pbpk.pi_kidney/pbpk.P_kidney ...
    pbpk.kidney_flow]/pbpk.blood_kidney;

% b. kidney tissue
A(8,7:8)=pbpk.pi_kidney*[1 -1/pbpk.P_kidney]/pbpk.kidney_volume;

%% eqn::Bladder [9,10]

% a. bladder blood
A(9,[9 10 13])= [-pbpk.pi_bladder-pbpk.bladder_flow ...
    pbpk.pi_bladder/pbpk.P_bladder ...
    pbpk.bladder_flow]/pbpk.blood_bladder;

% b. bladder tissue
A(10,9:10)=pbpk.pi_bladder*[1 -1/pbpk.P_bladder]/pbpk.bladder_volume;
%% eqn::Residual [11,12]

% a. residual blood
A(11,11:13) = [-pbpk.rest_flow-pbpk.pi_rest ...
    pbpk.pi_rest/pbpk.P_rest ...
    pbpk.rest_flow]/pbpk.blood_rest;

% b. residual tissue
A(12,11:12) = pbpk.pi_rest*[1 -1/pbpk.P_rest]/pbpk.rest_volume;

%% eqn::Lung [13,14]
% a. lung bulk
A(13,:) = [pbpk.cardiac_output ...
           zeros(1,11) ...
          -pbpk.cardiac_output - pbpk.pi_lung ...
           pbpk.pi_lung/pbpk.P_lung] / pbpk.blood_lung;

% b. lung tissue
A(14,13:14)=pbpk.pi_lung*[1 -1/pbpk.P_lung]/pbpk.lung_volume;

%% System

% labels:

% a. Check whether the continuous time system is observable by SVD
[~, So]=svd(obsv(A,C));
obsRank = sum(diag(So)>eps);
distNonObs = min(diag(So));

if (obsRank<14)
    error('The pair (C,A) is not observable!');
end

[~,Sc]=svd(ctrb(A,B));
ctrbRank = sum(diag(Sc)>eps);
distNonCtrb = min(diag(Sc));

% b. Discretization
csys = ss(A,B,C,D,'inputname','ivrate','statename',...
    {'Cpl','Crbc','Cvs','Cs','Cvliv','Cliv','Cvkid','Ckid','Cvblad','Cblad','Cvr','Cr','Cart','Clu'}, ...
    'outputname','Cplasma');
%T=0.00034;
T=0.01;
dsys = c2d(csys,T,'zoh');

dA=dsys.A;
dB=dsys.B;
dC=dsys.C;
dD=dsys.D;

[~, SDo]=svd(obsv(dsys.a,dsys.c));
obsRankD = sum(diag(SDo)>eps);
distNonObsD = min(diag(SDo));
%% Observer Design
[~,L] = kalman(dsys,40,1,0);
kalA=dA-L*dC;
kalB=[L dB];
kalC=eye(14);
kalD=zeros(14,2);
