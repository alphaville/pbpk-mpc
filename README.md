# PBPK Simulator

**Model Predictive Control of the Intravenous Administration
of Drugs - Simulation Framework.**



### Instructions

1. Download and install [QPC](http://sigpromu.org/quadprog/) * - add the files to the path of MATLAB*
2. Download and install [YALMIP](http://users.isy.liu.se/johanl/yalmip/pmwiki.php?n=Main.Download) 
3. Run `PBPK.m` to load the data
4. Run `pbpk_control.mdl` in Simulink 
   [alternatively run `sim('pbpk_control');` this will not fire up a Simulink window]
5. Run `plotter.m` to plot the results.


*Remark: _Alternatively any other QP solver can be used, e.g., CPLEX, but then in getInput.m line:65 it has to be specified which solver is to be used by YALMIP._



### Files

* __PBPK.m__: Initializes the parameters
* __pbpk_init.m__: Is a utility function that is called by PBPK.m and constructs a whole-body PBPK model for a given hematocrit and body weight
* __pbpk_state.m__: Returns a state space representation (LTI) of a PBPK model
* __pbpk_perturb.m__: Constructs a randomly perturbed variant of a PBPK model
* __mpcmat.m__: Constructs some matrices that are necessary for the formulation of the MPC problem as a QP
* __controller.m__: The MPC controller
* __getInput.m__: Utility function called by MPC controller
* __PBPK_overall.m__: This file offers an alternative procedure to construct the state space representation of a PBPK model (For testing purposes only).
* __plot_observer.m__: Plot the responses of the observer and the system in a common plot
* __plotter.m__: Plots the trajectories of the system (concentrations and administration rate)
* __evaluation.m__: Evaluates the closed loop behaviour of the controller system in presence of the MPC controller on a random population of patients created by pbpk_perturb.m
* __tracking.m__: Computes the tracking reference for offset-free MPC
* __pbpk_control.mdl__: Main Simulink file. This is the main simulator in which the user can choose to either run closed-loop simulaitons or just make use of the observer.